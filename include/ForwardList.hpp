// class ForwardList header file

#pragma once
#include <string>


struct Node {
    int data;
    Node* next;
};


class ForwardList {
    Node* start;
public:
    ForwardList(int, int*);
    ~ForwardList();
    auto insert(int) -> void;
    auto remove(int) -> int;
    auto print() const -> std::string;
    auto search(int) const -> std::string;
    auto change(int, int) -> int;
    auto sort() const -> int;
};
