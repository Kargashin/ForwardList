// Header file for TUI class

#pragma once
#ifdef _WIN32
#include <windows.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#endif
#include <iostream>
#include <string>
#include <sstream>


namespace TUI {
enum { DEFAULT, BLUE, GREEN, RED, CYAN };
auto change_color(int) -> void;
auto show_menu() -> void;
auto print_error(const std::string&) -> void;
auto print_f(const std::string&) -> bool;
auto print_ln(const std::string&) -> bool;
auto print_confirm(const std::string&) -> void;
auto input(std::istringstream&) -> int;
auto input(std::istream&) -> int;
auto init_input(int&, char**) -> int*;
auto str_input(std::istream&) -> std::string;
auto is_exit() -> int;
auto user_exit() -> void;
};
