VER = -std=c++14
CCX = g++
srcDIR = ./src/
incDIR = ./include/
objDIR = ./obj/

all: file list
	@echo "Запустите программу с помощью $ ./list <список элементов>"

file:
	@mkdir -p obj
	@echo "Создание директории /obj/"

list: main.o TUI.o ForwardList.o
	@$(CCX) $(objDIR)main.o $(objDIR)TUI.o $(objDIR)ForwardList.o -o list $(VER)
	@echo "Линковка объектных файлов.."

TUI.o: $(srcDIR)TUI.cpp $(incDIR)TUI.hpp 
	@$(CCX) $(srcDIR)TUI.cpp -c -o $(objDIR)TUI.o $(VER) -include$(incDIR)TUI.hpp
	@echo "Создание TUI.o.."

ForwardList.o: $(srcDIR)ForwardList.cpp $(incDIR)ForwardList.hpp 
	@$(CCX) $(srcDIR)ForwardList.cpp -c -o $(objDIR)ForwardList.o $(VER) -include$(incDIR)ForwardList.hpp
	@echo "Создание ForwardList.o.."

main.o: $(srcDIR)main.cpp $(incDIR)TUI.hpp $(incDIR)ForwardList.hpp
	@$(CCX) $(srcDIR)main.cpp $(VER) -c -o $(objDIR)main.o -include$(incDIR)TUI.hpp -include$(incDIR)ForwardList.hpp
	@echo "Создание main.o.."
	

clean:
	@rm -rf $(objDIR)
	@echo "Объектные файлы удалены!"

uninstall: 
	@rm -rf list $(objDIR)
	@echo "Программа удалена!"
