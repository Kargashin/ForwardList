// Source file for ForwardList class


auto ForwardList::insert(int val) -> void {
    if (!start) {
        start = new Node{val, nullptr};
        return;
    }

    Node* curr = start;

    while (curr -> next != nullptr)
        curr = curr -> next;

    curr -> next = new Node{val, nullptr};
};


auto ForwardList::remove(int val) -> int {
    if (!start) {
        return -1;
    }

    Node* curr = start;
    Node* prev = curr;

    if (curr -> data == val) {
        curr = start -> next;
        delete start;
        start = curr;
        return 1;
    }

    curr = curr -> next;

    while (curr != nullptr) {
        if (curr -> data == val) {
            prev -> next = curr -> next;
            delete curr;
            curr = prev -> next;
            return 1;
        } else {
            curr = curr -> next;
            prev = prev -> next;
        }
    }

    return 0;
};


auto ForwardList::print() const -> std::string {
    if (!start) {
        return "";
    }

    std::string str = "";
    Node* curr = start;
    str += std::to_string(start -> data);
    curr = curr -> next;

    while (curr != nullptr) {
        str += " -> " + std::to_string(curr -> data);
        curr = curr -> next;
    }

    return str;
};


ForwardList::~ForwardList() {
    Node* curr = start;

    while (curr != nullptr) {
        curr = curr -> next;
        delete start;
        start = curr;
    }
}


ForwardList::ForwardList(int count, int* inp) {
    if (!count) {
        start = nullptr;
        return;
    }

    start = new Node {inp[0], nullptr};
    Node* curr = start;

    for (int i = 1; i < count; i++) {
        Node* tmp = new Node {inp[i], nullptr};
        curr -> next = tmp;
        curr = tmp;
    }
}


auto ForwardList::search(int val) const -> std::string {
    if (!start) {
        return "";
    }

    Node* curr = start;
    int pos = 0;
    std::string str = "";

    while (curr != nullptr) {
        if ((curr -> data) == val) {
            str += std::to_string(pos) + " ";
        }

        curr = curr -> next;
        pos++;
    }

    if (str.empty()) {
        str = "Элемент " + std::to_string(val) + " отсутствует!";
    }

    return str;
}


auto ForwardList::change(int val, int pos) -> int {
    if (!start) {
        return -1;
    }

    Node* curr = start;
    int curr_pos = 0;

    while (curr != nullptr) {
        if (pos == curr_pos) {
            curr -> data = val;
            return 1;
        }

        curr = curr -> next;
        curr_pos++;
    }

    return 0;
}


auto ForwardList::sort() const -> int {
    if (!start) {
        return -1;
    }

    Node* curr = start;
    Node* follow;

    for (Node* i = start; i -> next != nullptr; i = i -> next) {
        for (Node* j = i -> next; j != nullptr; j = j -> next) {
            if ((i -> data) > (j -> data)) {
                std::swap(i -> data, j -> data);
            }
        }
    }

    return 0;
}