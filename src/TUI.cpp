// Source file for TUI namespace


auto TUI::change_color(int c) -> void {
#ifdef _WIN32
    HANDLE hConsole = GetStdHandle(STD_std::cout_HANDLE);
#endif
    switch (c) {
    case BLUE:
#if defined(__gnu_linux__) || (defined(__APPLE__) && defined(__MACH__))
        std::cout << "\033[34m";
#endif
#ifdef _WIN32
        SetConsoleTextAttribute(hConsole, (WORD)((0 << 4) | 3));
#endif
        break;
    case GREEN:
#if defined(__gnu_linux__) || (defined(__APPLE__) && defined(__MACH__))
        std::cout << "\033[32m";
#endif
#ifdef _WIN32
        SetConsoleTextAttribute(hConsole, (WORD)((0 << 4) | 10));
#endif
        break;
    case DEFAULT:
#if defined(__gnu_linux__) || (defined(__APPLE__) && defined(__MACH__))
        std::cout << "\033[39;49m";
#endif
#ifdef _WIN32
        SetConsoleTextAttribute(hConsole, (WORD)((0 << 4) | 15));
#endif
        break;
    case RED:
#if defined(__gnu_linux__) || (defined(__APPLE__) && defined(__MACH__))
        std::cout << "\033[31m";
#endif
#ifdef _WIN32
        SetConsoleTextAttribute(hConsole, (WORD)((0 << 4) | 4));
#endif
        break;
    case CYAN:
#if defined(__gnu_linux__) || (defined(__APPLE__) && defined(__MACH__))
        std::cout << "\033[36m";
#endif
#ifdef _WIN32
        SetConsoleTextAttribute(hConsole, (WORD)((0 << 4) | 11));
#endif
    }
}


auto TUI::show_menu() -> void {
    change_color(DEFAULT);
    std::cout << "Выберите одну из операций:\n";
    change_color(CYAN);
    std::cout << "1. Распечатать список\n"
    << "2. Добавить элементы в список\n"
    << "3. Удалить элемент\n"
    << "4. Найти позиции элементов\n"
    << "5. Заменить элемент на другой\n"
    << "6. Отсортировать элементы списка\n"
    << "7. Завершить работу программы\n";
}


auto TUI::print_error(const std::string& text) -> void {
    change_color(RED);
    std::cout << text << std::endl;
}


auto TUI::print_f(const std::string& text) -> bool {
    if (text.empty())
        return false;
    change_color(DEFAULT);
    std::cout << text;
    return true;
}


auto TUI::print_ln(const std::string& text) -> bool {
    if (text.empty())
        return false;
    change_color(DEFAULT);
    std::cout << text << std::endl;
    return true;
}


auto TUI::print_confirm(const std::string& text) -> void {
    change_color(GREEN);
    std::cout << text << std::endl;
}


auto TUI::input(std::istringstream& inp) -> int {
    change_color(GREEN);
    int c;
    inp >> c;
    inp.get();

    return c;
}


auto TUI::input(std::istream& inp) -> int {
    change_color(GREEN);
    int c;

    while (!(inp >> c)) {
        print_error("Ошибка ввода!");
        inp.clear();
        while (inp.get() != '\n');
        change_color(GREEN);
    }

    return c;
}


auto TUI::init_input(int& count, char** argv) -> int* {
    if (count == 1) {
        count--;
        return nullptr;
    }

    std::string row;
    std::size_t pos = -1;
    std::size_t pos_h = 0;
    int* values;

    for (int i = 1; i < count; i++) {
        row += std::string(*(argv + i)) + " ";
    }
    count--;

    if (count == 1) {
        for (int i = 0; i < row.length(); i++) {
            if (row[i] == ',') count++;
        }
    }

    values = new int[count];
    std::istringstream inp(row);
    for (int i = 0; i < count; i++) {
        values[i] = input(inp);
    }

    return values;
}

auto TUI::str_input(std::istream& inp) -> std::string {
    std::string str;
    change_color(GREEN);
    std::getline(inp, str);

    return str;
}


auto TUI::is_exit() -> int {
    std::string ex;
    print_f("Вы хотите выйти из программы? (y/N) ");
    std::cin.get();
    ex = str_input(std::cin);

    if (ex == "y" || ex == "yes" || ex == "Y" || ex == "Yes" || ex == "YES") {
        return 1;
    }

    if (ex != "n" && ex != "no" && ex != "N" && ex != "No" && ex != "NO") {
        return -1;
    }

    return 0;
}


auto TUI::user_exit() -> void {
    change_color(DEFAULT);
}