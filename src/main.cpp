// main.cpp


int main(int argc, char* argv[]) {
    setlocale(LC_ALL, "RUS");

    int* values = TUI::init_input(argc, argv);
    ForwardList arr{argc, values};
    delete[] values;

    int choice;
    int help;
    bool exit = false;

    while(1) {
        if (exit) break;
        TUI::show_menu();
        choice = TUI::input(std::cin);
        switch (choice) {
        case 1:
            if (!TUI::print_ln(arr.print())) {
                TUI::print_error("Список пуст!");
            }
            break;

        case 2:
            TUI::print_ln("Введите элементы:");
            arr.insert(TUI::input(std::cin));
            while(std::cin.peek() != '\n') {
                arr.insert(TUI::input(std::cin));
            }
            break;

        case 3:
            TUI::print_f("Выберите значение элемента: ");
            if ((choice = arr.remove(TUI::input(std::cin))) == -1 || choice == 0) {
                choice == 0 ? TUI::print_error("Элемент отсутствует!") : TUI::print_error("Список пуст!");
            }
            break;

        case 4:
            TUI::print_f("Введите значение элемента: ");
            if (!TUI::print_ln(arr.search(TUI::input(std::cin)))) {
                TUI::print_error("Список пуст!");
            }
            break;

        case 5:
            TUI::print_f("Введите позицию и новое значение: ");
            if ((choice = arr.change(TUI::input(std::cin), help = TUI::input(std::cin))) == -1 || choice == 0) {
                choice == 0 ? TUI::print_error("Элемент c позицией " + std::to_string(help) + " отсутствует!") : TUI::print_error("Список пуст!");
            }
            break;

        case 6:
            if (arr.sort()) {
                TUI::print_error("Список пуст!");
            }
            break;

        case 7:
            if ((choice = TUI::is_exit()) == -1) {
                TUI::print_error("Неверный ответ!");
            } else if (choice == 1) {
                TUI::print_confirm("До свидания!");
                exit = true;
            }
            break;

        default:
            TUI::print_error("Нет такой команды!");
        }
    }

    TUI::user_exit();
}
